Nginx
=======

This role runs a docker instance of nginx with virtualhost. The virtual host could be either in http or https. 

# Sample config:

```
- name: set nginx role
  hosts: servers
  gather_facts: true
  remote_user: root
  vars:
    - vhost: testing.gomcomm.com
    - upstream_host: localhost
    - port: 9200
    - state: present                            # either present or absent
    - type: https                               # either https or http

    ### for https only ###
    - remote_cert_path: /etc/ssl/certs/{{ vhost }}
    - remote_key_path: /etc/ssl/private/{{ vhost }}

    ### optional, if specified will be copied over at their respective remote paths and may **OVERWRITE** any existing key/cert there 
    - local_cert_path: /tmp/ssl/server.crt
    - local_key_path: /tmp/ssl/server.key

  roles:
  - nginx

```
